+++
title = "Strange Things"
date = 2020-02-08
description = "Why software engineering can hurt you and how to deal with it."
+++

How many times a week you spent your energy fighting environment instead of getting
help from it?

<!-- more -->

## Strange things

I decided to check why `notifications` project's CI failed during tests execution. 
First assumption was that something was wrong with tests.
I rewrote them to use real structures, instead of dummy arrays, but it didn't changed the CI's behaviour.
Something failed and I didn't know what and why.
I decided to clear caches, because target folder contains them. Still failing.

```
running 2 tests
686 test messaging::test_client_connect_ok ... FAILED
687 test messaging::test_client_subscrib_receive_message ... thread '<unnamed>' panicked at 'called `Result::unwrap()` on an `Err` value: "SendError(..)"', src/libcore/result.rs:1188:5
688 Notification Event DeliveredMessage { subject: "notifications", subscription_id: 2, reply_to: None, payload_size: 37, payload: [123, 34, 99, 104, 97, 116, 95, 105, 100, 34, 58, 52, 49, 48, 51, 48, 57, 48, 44, 34, 109, 101, 115, 115, 97, 103, 101, 34, 58, 34, 84, 101, 115, 116, 46, 34, 125] }.
689 ok
690 failures:
691 ---- messaging::test_client_connect_ok stdout ----
692 thread 'main' panicked at 'Failed to connect Client.: Error { kind: Timeout, description: Some("Failed to establish connection without timeout") }', src/libcore/result.rs:1188:5
693 note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace.
694 failures:
695     messaging::test_client_connect_ok
696 test result: FAILED. 1 passed; 1 failed; 0 ignored; 0 measured; 0 filtered out
697 error: test failed, to rerun pass '--bin notifications'
```

Kind of a magic. Locally everything works fine.
I decided to change CI's build image to `nigtlhy`. Build time is real showstopper by the way.
Nothings changed.

### Thoughts on CI

Differences between local and CI configurations. I think they should be more similar.
Right now I use DIND on CI + Rust image to build my projects.
Locally I have docker compose with ports' exports to host OS.
I should use containers for an environment in both situations, but in a more common way than now.
I also have conflicts in CI configurations and should find the way to place them in one space
and just inject or autocopy into each project.
First layer - library. Build, Formatting, Clippy, Tests, Dependencies scan, Publish as lib.
Second layer - application. All from the first layer, package to container image, test image.

### Back to my sheap

Nightly didn't change the situation. But wait a minute.
I already faced problem with unexpected environments and decided to check twice 
before expecting that everything set as needed. All good. I'm currently ran out of ideas.
No errors - only *timeout*.
Wait a minute - maybe it's really a CI problem with a latency!? Will check it.
Da blyati - really - I had `.connect_timeout(std::time::Duration::from_millis(1000))` in `practicqrs`
and not in the `notifications`.
I should really exctract infrastructure to another common library.
Oh, such an idiot.

## TryFrom

Today ([Strange Things](#strange-things) were technically this night) I decided to remove boilerplate
in form of converting `serde_json::from_slice(&msg.payload).expect("Failed to deserialize message.")`
into TelegramNotification struct each time using `serde` directly. So I need a way to put it into more sophisticated form.
Because I try to be a good developer, I will not reinvent the wheel and will use existing [`From` trait](https://doc.rust-lang.org/std/convert/trait.From.html).
But because `from_slice` method is *fallible* I need to use not a simple `From`, I need `TryFrom`.

[Results here](https://gitlab.com/practic-47/practic-events-lib/-/merge_requests/5).
