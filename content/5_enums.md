+++
title = "Enum"
date = 2020-02-15
description = "Hello enum my old friend."
+++

Several years ago I wrote small [Diesel+Rocket](https://github.com/humb1t/rpom/) and faced the problem with enums mapping.

<!-- more -->

2020 - I faced the same task and looked through old solution, maybe you will also need it:
1. Add `https://github.com/adwhit/diesel-derive-enum` to your dependencies.
1. Add enums to sql.
1. [Pimp](https://github.com/humb1t/rpom/blob/338871ca5515fc8cc5d1214aaa8d3d7a24069ed2/diesel.toml) your `diesel.toml`.

## What

By default there is no mapping between enums and SQL types, `diesel-derive-enum` will generate it.
In PostgreSQL you should define your enum types and use them in tables (internally they will have integer values).
And because diesel generates schema based on migrations, generated enum type should be available for them via imports 
in `diesel.toml`.

## Result

[MR](https://gitlab.com/practic-47/practicqrs/-/merge_requests/25).
