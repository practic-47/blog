+++
title = "Don't Stop"
date = 2020-02-09
description = "Why sometimes you should keep rolling."
+++

Yesterday CI failed to publish new crate because some tricky aspects of GitLab.

<!-- more -->

## Dependencies and where to find them 

Thanks Lord we can use `dep_name = { git = "https://..." }` in `Cargo.toml` and don't worry about for now.
Because we should keep moving forward, I already spent weekends optimizing code, it's time to build new features.


Today we will add ability to delete courses and maybe something else.

## Autocomplete and IDE

I like to use VIM for Rust coding, today in our [local Rust chat](t.me/ruts_chat) we discussed IDE vs VIM,
do we need autocompletion etc. In this discussion I realized that an ability of Rust to
define local `uses` is quite improtant for my style:
```rust
                  use crate::schema::courses::dsl::*;
                  HttpResponse::Ok().json(
                      courses
                          .load::<Course>(&data.connection)
                          .expect("There is problem with loading courses"),
                  )
```

While many of you will think that:
1. local declarations are silly
1. why I need to remember methods names
I prefer to do such a things locally (gives more context to the reader) and manually (require more 
knowledge about the context and API from writer).

I wrote words above and switched to code. I need to choose HTTP response status to delete action.
```rust
          .route(
              "/courses/",
              web::delete().to(|data: web::Data<Environment>| {
                  use crate::schema::courses::dsl::*;
>>                  HttpResponse::
              }),
          )
```
What I will do? No `ctrl+space` magic - just `https://docs.rs`.
And now, I will think twice what to return, will check that it's a standard way to tell (all good, resource was deleted).
MDN doesn't have an answer to that question. [Swagger](https://restfulapi.net/http-methods/#delete) gives good info.

That's why I do things manually. It gives me a slow flow where I can have a moments to think without rush (almost wrote rust here).

## Overengineering

When I started project usage of NATS to send message into itself from service was fun, but with a concept
of stateless REST API creation of a resources without immediate feedback shot me in a foot.

Now I decided to write delete functionality and guess what? It's quite tricky to get an ID of created enitity.
So I will throw overengineering away and will do such techniques when they need to be done.

Result [here](https://gitlab.com/practic-47/practicqrs/-/merge_requests/22).

## Night before the christmas

So I wasn't ready that nightly rust can lack some tools like Rust fmt and clippy. So I made some hacks in my CI config to see how it
will handle different checks and different environments (stable/nightly).
