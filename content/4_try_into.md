+++
title = "Try Into"
date = 2020-02-14
description = "Strange day, strange problems."
+++

Today was a strange day, I missed a rebase and faced another problem.

<!-- more -->

`Practic-events-lib` contains this `TryInto` trait impl:
```rust
impl TryFrom<Vec<u8>> for TelegramNotification {
    type Error = serde_json::Error;

    fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
        serde_json::from_slice(&value)
    }
}
```
but when I want to use `try_into` in `practicqrs` it fails with:
```
error[E0277]: the trait bound `std::vec::Vec<u8>: std::convert::From<practic_events_lib::TelegramNotification>` is not satisfied
  --> src/ordering.rs:94:10
   |
94 |         .try_into()
   |          ^^^^^^^^ the trait `std::convert::From<practic_events_lib::TelegramNotification>` is not implemented for `std::vec::Vec<u8>`
   |
   = help: the following implementations were found:
             <std::vec::Vec<T> as std::convert::From<&[T]>>
             <std::vec::Vec<T> as std::convert::From<&mut [T]>>
             <std::vec::Vec<T> as std::convert::From<std::borrow::Cow<'a, [T]>>>
             <std::vec::Vec<T> as std::convert::From<std::boxed::Box<[T]>>>
           and 6 others
   = note: required because of the requirements on the impl of `std::convert::Into<std::vec::Vec<u8>>` for `practic_events_lib::TelegramNotification`
   = note: required because of the requirements on the impl of `std::convert::TryFrom<practic_events_lib::TelegramNotification>` for `std::vec::Vec<u8>`
   = note: required because of the requirements on the impl of `std::convert::TryInto<std::vec::Vec<u8>>` for `practic_events_lib::TelegramNotification`

error: aborting due to previous error

For more information about this error, try `rustc --explain E0277`.
error: could not compile `practicqrs`.

To learn more, run the command again with --verbose.
```

Quite strange. Looks like custom error type in `TryFrom` ruins attempts to derive into.

## Playground

Let's check this.

First [simulation](https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=6d1112273a6af46740e25fe2467f5ea5) of our example.
I used error type from [Rust Std Doc](https://doc.rust-lang.org/std/convert/trait.TryFrom.html#generic-implementations).
Result:
```
  Compiling playground v0.0.1 (/playground)
error[E0277]: the trait bound `std::vec::Vec<u8>: std::convert::From<std::result::Result<main::TelegramNotification, &str>>` is not satisfied
  --> src/main.rs:22:44
   |
22 |         let vector: Vec<u8> = notification.try_into().unwrap();
   |                                            ^^^^^^^^ the trait `std::convert::From<std::result::Result<main::TelegramNotification, &str>>` is not implemented for `std::vec::Vec<u8>`
   |
   = help: the following implementations were found:
             <std::vec::Vec<T> as std::convert::From<&[T]>>
             <std::vec::Vec<T> as std::convert::From<&mut [T]>>
             <std::vec::Vec<T> as std::convert::From<std::borrow::Cow<'a, [T]>>>
             <std::vec::Vec<T> as std::convert::From<std::boxed::Box<[T]>>>
           and 5 others
   = note: required because of the requirements on the impl of `std::convert::Into<std::vec::Vec<u8>>` for `std::result::Result<main::TelegramNotification, &str>`
   = note: required because of the requirements on the impl of `std::convert::TryFrom<std::result::Result<main::TelegramNotification, &str>>` for `std::vec::Vec<u8>`
   = note: required because of the requirements on the impl of `std::convert::TryInto<std::vec::Vec<u8>>` for `std::result::Result<main::TelegramNotification, &str>`

error: aborting due to previous error

For more information about this error, try `rustc --explain E0277`.
error: could not compile `playground`.

To learn more, run the command again with --verbose.
```

Looks familar. Let's use Infallibale as an error type?
```
   Compiling playground v0.0.1 (/playground)
error[E0277]: the trait bound `std::vec::Vec<u8>: std::convert::From<std::result::Result<main::TelegramNotification, std::convert::Infallible>>` is not satisfied
  --> src/main.rs:22:44
   |
22 |         let vector: Vec<u8> = notification.try_into().unwrap();
   |                                            ^^^^^^^^ the trait `std::convert::From<std::result::Result<main::TelegramNotification, std::convert::Infallible>>` is not implemented for `std::vec::Vec<u8>`
   |
   = help: the following implementations were found:
             <std::vec::Vec<T> as std::convert::From<&[T]>>
             <std::vec::Vec<T> as std::convert::From<&mut [T]>>
             <std::vec::Vec<T> as std::convert::From<std::borrow::Cow<'a, [T]>>>
             <std::vec::Vec<T> as std::convert::From<std::boxed::Box<[T]>>>
           and 5 others
   = note: required because of the requirements on the impl of `std::convert::Into<std::vec::Vec<u8>>` for `std::result::Result<main::TelegramNotification, std::convert::Infallible>`
   = note: required because of the requirements on the impl of `std::convert::TryFrom<std::result::Result<main::TelegramNotification, std::convert::Infallible>>` for `std::vec::Vec<u8>`
   = note: required because of the requirements on the impl of `std::convert::TryInto<std::vec::Vec<u8>>` for `std::result::Result<main::TelegramNotification, std::convert::Infallible>`

error: aborting due to previous error

For more information about this error, try `rustc --explain E0277`.
error: could not compile `playground`.

To learn more, run the command again with --verbose.
```
The same result. Let's remove error type. Nope - it's required. So let's write From instead of TryFrom.

```
   Compiling playground v0.0.1 (/playground)
error[E0277]: the trait bound `std::vec::Vec<u8>: std::convert::From<main::TelegramNotification>` is not satisfied
  --> src/main.rs:20:44
   |
20 |         let vector: Vec<u8> = notification.try_into().unwrap();
   |                                            ^^^^^^^^ the trait `std::convert::From<main::TelegramNotification>` is not implemented for `std::vec::Vec<u8>`
   |
   = help: the following implementations were found:
             <std::vec::Vec<T> as std::convert::From<&[T]>>
             <std::vec::Vec<T> as std::convert::From<&mut [T]>>
             <std::vec::Vec<T> as std::convert::From<std::borrow::Cow<'a, [T]>>>
             <std::vec::Vec<T> as std::convert::From<std::boxed::Box<[T]>>>
           and 5 others
   = note: required because of the requirements on the impl of `std::convert::Into<std::vec::Vec<u8>>` for `main::TelegramNotification`
   = note: required because of the requirements on the impl of `std::convert::TryFrom<main::TelegramNotification>` for `std::vec::Vec<u8>`
   = note: required because of the requirements on the impl of `std::convert::TryInto<std::vec::Vec<u8>>` for `main::TelegramNotification`

error: aborting due to previous error

For more information about this error, try `rustc --explain E0277`.
error: could not compile `playground`.

To learn more, run the command again with --verbose.
```

WTF?
Even without a Vec:
```
   Compiling playground v0.0.1 (/playground)
error[E0277]: the trait bound `u8: std::convert::From<main::TelegramNotification>` is not satisfied
  --> src/main.rs:19:39
   |
19 |         let vector: u8 = notification.into();
   |                                       ^^^^ the trait `std::convert::From<main::TelegramNotification>` is not implemented for `u8`
   |
   = help: the following implementations were found:
             <u8 as std::convert::From<bool>>
             <u8 as std::convert::From<std::num::NonZeroU8>>
   = note: required because of the requirements on the impl of `std::convert::Into<u8>` for `main::TelegramNotification`

error: aborting due to previous error

For more information about this error, try `rustc --explain E0277`.
error: could not compile `playground`.

To learn more, run the command again with --verbose.
```

## Numb

I missed the key error in my examples. Thanks `@gsomix` he make it clear for me:
>> From<T> for U implies Into<U> for T

I made TryFrom<Vec<u8>> for TelegramNotification and got Into<TelegramNotification> for Vec<u8>, but
was waiting Into<Vec<u8>> for TelegramNotification - I think I expect more than Rust can give to the world :-)

So the fix was made via [this MR](https://gitlab.com/practic-47/practic-events-lib/-/merge_requests/10).

## Read carefully

So to don't be as dumb as I am - read carefully, play with examples and be good.
