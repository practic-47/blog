+++
title = "Tests"
date = 2020-02-16
description = "Write tests."
+++

When you work with web framework, tests is what you should be able to write easily with them.

<!-- more -->

Sometimes it's easy to write tests without specific configurations, deploy test instance and 
work with it via http.

Sometimes frameworks provide you with tools for testing (like Spring in Java world and Actix in Rust).
Today I faced an issue with the last one.

```rust
          then "Order in `PENDING` state created" |world,_| {
              use std::convert::TryFrom;
              println!("Response {:?}", world.response);
              let response = world.response.as_mut().expect("Response is missing.");
              assert!(response.status().is_success());
              let body = response.take_body().as_ref().expect("Failed to retrieve response body.");
>>            Order::try_from(body.0);
          };
```

I need to check response's body, but I saved the response using: 
```rust
                  test::block_on(
                      world.service.call(
                          test::TestRequest::put()
                              .uri("/ordering/courses/1/orders/")
                              .to_request()
                      )
                  ).unwrap()
```
and it has a type: [ServiceResponse](https://docs.rs/actix-web/1.0.9/actix_web/dev/struct.ServiceResponse.html)
which can give you status [directly](https://docs.rs/actix-web/1.0.9/actix_web/dev/struct.ServiceResponse.html#method.status)
but it's hard to work with body.

You can easily receive body using [read_response_json](https://docs.rs/actix-web/1.0.9/actix_web/test/fn.read_response_json.html) 
helper function, but you will miss other information.

If you will go  my way you will need to call `take_body` and receive [ResponseBody](https://docs.rs/actix-web/1.0.9/actix_web/dev/enum.ResponseBody.html).
After that you can try to figure out how to convert it into json (or at least bytes).

Or you can...

## Rewrite everything

Yeah, almost. I still will use actix, but for integration tests I will spin up real server and use http-library.

I decided to check `http://www.arewewebyet.org/` and choose nice in terms of syntax lib.

So things I needed to do:
- Rewrite World's server
- Rewrite Step Definitions
- Check that parallel execution works fine locally and on CI

The first problem is `service: actix_web::HttpServer<actix_http::Request, dev::ServiceResponse, error::Error>,`
definition of the server struct to keep it in the world. But wait - I need only address!

While I was making changes, I think about coupling between tests and frameworks. In terms of BDD integration tests
it's much easier to do not use any framework specific. While for unit tests it's easier to use frameworks.

Also a funny part that `assert!(world.response.as_ref().unwrap().status().is_success());` stayed the same for `isahc`.
API of this library is really good.
`isahc::get("/ordering/orders/").expect("Failed to retrieve orders.")` to receive response,
`response.json::<Order>()` to deserialize.

The challenge is to start actix server and do not block test execution thread. I decided to just spawn a new thread.
Also I disabled HTTPS for now, need to reconfigure default client or get trusted certificates :-)

[Results](https://gitlab.com/practic-47/practicqrs/-/merge_requests/26).
