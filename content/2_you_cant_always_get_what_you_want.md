+++
title = "You Can't always get what you want."
date = 2020-02-09
description = "Why sometimes you will reinvent the wheel."
+++

Yesterday, all my troubles seemed so far away

Now it looks as though they're here to stay

Oh, I believe in yesterday

<!-- more -->

## Magic Types 

How smart is your compiler? Rust's compiler very smart, but he speaks totally different language than me.

This morning I was trying to refactor this:
```rust
serde_json::to_vec(&message).expect("Failed to serialize message")
```
into something like this:
```rust
&message.to_vec()
```

Because yesterday I made a `TryFrom` impl for `TelegramNotification` I was believe in the following solution:
```rust
message.try_into()
```
But signature of a consuming method was waiting for `&[u8]` - while my impl was for `Vec<u8>`.
It also has problems with unknown size of an array.

I decided to extract local variable with type `Vec<u8>`:
```rust
let message: Vec<u8> = TelegramNotification{...}.try_into()...
```
But it failed. It was waiting that I have not a `TryFrom`, but `From`.
I don't have time for now to understand why `TryFrom` and `TryInto` are not reversible.

### What in the end

```rust
impl TelegramNotification {
    fn to_vec(&self) -> Vec<u8> {
        serde_json::to_vec(&self).expect(&format!(
            "Failed to serialize TelegramNotification: {}",
            &self
        ))
    }
}
```

Maybe it even better, because I also encapsulated `expect` in this method. At the same time, 
my application can fail because of this `expect`. But how it's possible to fail - IDK.
It's unreachable fail, but my program didn't proof it to the compiler.

[Resulted MR](https://gitlab.com/practic-47/practic-events-lib/-/merge_requests/8) and
[issue](https://gitlab.com/practic-47/practic-events-lib/issues/1).

## Publicity and Toxicityyyyy

When I merged the MR and updated dependencies on my `notifications` project I mentioned that 
`method 'to_vec' is private` error was preventing me from using a new API.
Why tests didn't show me that? They were in the same crate. 

So I made another [MR](https://gitlab.com/practic-47/practic-events-lib/-/merge_requests/9)
with a public `to_vec` method and also moved all tests into `./tests/client.rs`.
Now when I write tests I look at the API by the eyes of the client.

### Release policy

I also want to mention that writing changelog I try to be aligned with SemVer. That's why despite 
the fact that `0.1.2` is the same as `0.1.1` for the clients I made `0.1.3` tag.

## Clean it up

Today I was making housekeeping and also decided to clean up some projects in parallel.

To make modules good in terms of cohesion and coupling I made some movements from top-level
main modules into domain specific ones.

### Imports

I also try to:

- make imports in one line style: `use actix_web::{web, App, HttpServer}`
- place them near usage and extract layer above only if needed

For what? I think it's easier to understand local context - so if you place all imports,
definitions and logic near each other you will make code more readable (tight coupling inside
  modules). But of course to make it small, you encapsulate logic inside other entities.
If you give such entities proper names, your code's readability won't suffer.

### Don't stop me now

I also decided to remove additional thread and run actix http server in the main thread.
```
     Running `target/debug/practicqrs`
Database migration finished.
[2020-02-09T17:04:22Z INFO  actix_web::middleware::logger] 127.0.0.1:58922 "GET /catalog/courses/ HTTP/2.0" 200 271218 "-" "Mozilla/5.0 (X
11; Linux aarch64; rv:68.0) Gecko/20100101 Firefox/68.0" 0.404472
^CHttpServer started.
^Ck
```

But in this cases it rejects to stop, why? I didn't find answer previosly but I want it now.
And the answer is simple - I had an actix's system run `let _ = sys.run()` without knowing why I need it.
It's a bad API in my opinion to be connascene (I hope I wrote it right) on the algorithmic level.
First - create system, then create something to be ran in the system and start the system.
Was very confusing, now I get the idea of actix and can clean up the mess. So you also can write some dirty code 
first, but remember to clean it up.

### From and Into

Looks pretty bad:
```rust
    fn new(request: CreateNewCourseRequest) -> Self {
        CreateNewCourseCommand {
            request: request,
            event: "CreateNewCourse".to_string(),
            created: Utc::now().to_rfc3339(),
        }
    }
```
how about that?
```rust
  impl std::convert::From<CreateNewCourseRequest> for CreateNewCourseCommand {
      fn from(request: CreateNewCourseRequest) -> Self {
          CreateNewCourseCommand {
              request: request,
              event: "CreateNewCourse".to_string(),
              created: Utc::now().to_rfc3339(),
          }
      }
  }
```
STD Trait, pure function, such clean.
Refactoring result will be available tommorow, I also want to extract `Messanger` into a new 
common library.
