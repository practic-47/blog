+++
title = "Hello, World!"
date = 2020-02-08
description = "All-in-one post with notes written before the blog run."
+++

Greetings, stranger!

<!-- more -->

Before this blog, [I](https://humb1t.gitlab.io/about/) was writing some notes in local `BLOG.md` file.
For the historical and some other purposes I decided to publish these notes as posts in this Blog.
First post will contain all previous notes, new posts would be separated by dates.

## Choosing Cloud Provider

| Cloud | Free Expiration Date | Free Device | 2x4 price |
| ---   | ---                  | ---         | ---       |
| https://www.ibm.com/cloud/free/ | Forever | 2 CPUs and 4 GB |
| https://aws.amazon.com/free/ | 12 Months | t2.micro |
| https://www.digitalocean.com/pricing/ | Not Free | | 20$/month |
| https://elastx.se/en/pricing/ | Not Free | | 86$/month |
| https://cloud.google.com/kubernetes-engine/ | 12 Months | several micro | |
| https://www.huaweicloud.com/en-us/pricing.html?tab=detail#/cce | not found | | 56 $/month |
| https://www.kazuhm.com/pricing/ | | 10 nodes | $20/node |
| https://www.linode.com/pricing/ | No | - | 20$/month |
| https://mcs.mail.ru/en/pricing/ | no | no | 1620P/month |
| https://www.ovh.ie/public-cloud/prices/ | no | no | €12.99 |
| https://www.openshift.com/products/online/ | Almost Forever | 1 project 2GiB | |
| https://www.scaleway.com/en/pricing/ | HZ | | 8 Euro |
| https://vexxhost.com/pricing/ | HZ | | 0.09/hr |

I decided to use Yandex Cloud for now. It have a good price, console and even Telegram working API,
despite the fact that VPS region is RU.

## GCP + GitLab

Because I like Cloud Native stuff, I decided to try GCP + GitLab integration for Rust stack.

1. "Perviy blin comom" - integration fails for the first time,
I was looking for a possible problems in Internet, but than just
cleaned up all integrations and you created a new one.
1. Installing helm failed with:
```
Error: error initializing: Looks like "https://kubernetes-charts.storage.googleapis.com" is not a valid chart repository or cannot be reached
```
[The only link](https://www.reddit.com/r/gitlab/comments/e6secu/problem_with_gitlab_and_k8s_cluster/f9uhlvs/)
about this problem was insufficient, I just recreated the integration and resources once again.
Then Helm installation failed with Nodes' tenants. Micro instances were not good for Helm.
So I changed 3 micro to 2 small - works fine.

### How to do it by your own

1. Register to Gitlab
1. Register to GCP, enable billing, create project
1. Go to Gitlab group or project, go to kubernetes tab, create new cluster
1. Install Helm, install Ingress, Prometheus.
1. ohuey ot tsen (5$ for two days with only infra for k8s) and delete this 

## Remote VPS

### Req

Minimum 2 GB RAM

### First boot 

#### Install docker

https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-engine---community

#### Install docker compose

https://docs.docker.com/compose/install/

#### Download compose file

https://gitlab.com/practic-47/configurations/-/blob/master/beta/docker-compose.yml

#### Run up -d

`docker-compose up -d`

## HTTPS connection to CQRS service

### Create certificate

`openssl req -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365 -subj '/CN=localhost'`

## Strugling with async ecosystem

### Trying to migrate to 2.0 actix

I decided to update actix to 2.0. Spent three hours, got some silent errors in tests, reverted to 1.0.

## How I would like to write Services in Rust

```rust
Server{
  resources: vec![
    Resource { uri: "/orders", endpoints: vec![Endpoint { uri: "/", method: HTTP::GET, function: Function::Sync(func1)}]}
  ],
}
```
