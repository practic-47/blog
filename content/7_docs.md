+++
title = "Docs"
date = 2020-02-16
description = "Write docs."
+++

Write docs, make them live, try to avoid maintanance.

<!-- more -->

## Why?

Because code is not enough. It may be written in a very good way to read, but suffer in terms of usage.
You should cover all corner cases via tests, but you will need to use business language.
It's easier to extract some descriptions into a human readable documentation, making code better
in terms of day to day usage and giving new users a chance to dive into logic via docs.

## How?

My favourite documentation technique is [described here](https://www.divio.com/blog/documentation/) and
promotes a categorization of docs by:
- Tutorials oriented on learning new things
- How-to Guides oriented on goals achieving 
- Explanations oriented on a deeper understanding 
- Reference oriented on giving accurate information

## Show me the docs

In Rust you have good docs via [Rust Docs](docs.rs).

For more business-oriented or domain-oriented point of view I've made [a book](https://practic-47.gitlab.io/practic-book/).
While it's still in WIP, you definetely can have a journey through it. All four categories are mixed up in this book.
Some chapters are made as tutorials, sometimes you will meet a guide and Appendixes are references.

Why Book? Because as I already said it is a journey. When you need a certain action, you can read Rust or another automatically
made documentation. The Book inspired by the Rust book and others (Haskell, Scala with cats, etc.) is an entrance into the
Practic's world. And it will let you to draw a map inside your head, not to remember every detail, but to understand
where to find answers.
